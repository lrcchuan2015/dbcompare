﻿using System;
using System.Windows;
using Dasic.Library;
using Dasic.Model;

namespace Dasic.Controls
{
    /// <summary>
    ///     DbCompareSet.xaml 的交互逻辑
    /// </summary>
    public partial class DbCompareSet : Window
    {
        private SQLiteDbComparse _rsDbComparse;

        public DbCompareSet()
        {
            InitializeComponent();
        }

        private void DbCompareSet_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _rsDbComparse = SqlHelper.SqLiteConnection.From<SQLiteDbComparse>().ToFirst();
                if (_rsDbComparse != null)
                {
                    TBoxMainHost.Text = _rsDbComparse.mip;
                    TBoxMainUser.Text = _rsDbComparse.muser;
                    TBoxMainPass.Password = _rsDbComparse.mpass;
                    TBoxMainPort.Text = _rsDbComparse.mport.ToString();
                    TBoxMainTable.Text = _rsDbComparse.mtable;
                    TBoxSlaveHost.Text = _rsDbComparse.sip;
                    TBoxSlaveUser.Text = _rsDbComparse.suser;
                    TBoxSlavePass.Password = _rsDbComparse.spass;
                    TBoxSlavePort.Text = _rsDbComparse.sport.ToString();
                    TBoxSlaveTable.Text = _rsDbComparse.stable;
                }
            }
            catch (Exception ex)
            {
                Common.Msg(false, "获取配置失败，信息：" + ex.Message);
            }
        }

        private void BtnSave_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                int rs;
                if (_rsDbComparse != null)
                    rs = SqlHelper.SqLiteConnection.Update(new SQLiteDbComparse
                    {
                        mip = TBoxMainHost.Text,
                        muser = TBoxMainUser.Text,
                        mpass = TBoxMainPass.Password,
                        mport = int.Parse(TBoxMainPort.Text),
                        mtable = TBoxMainTable.Text,
                        sip = TBoxSlaveHost.Text,
                        suser = TBoxSlaveUser.Text,
                        spass = TBoxSlavePass.Password,
                        sport = int.Parse(TBoxSlavePort.Text),
                        stable = TBoxSlaveTable.Text
                    }, d => d.id == _rsDbComparse.id);
                else
                    rs = SqlHelper.SqLiteConnection.Insert(new SQLiteDbComparse
                    {
                        mip = TBoxMainHost.Text,
                        muser = TBoxMainUser.Text,
                        mpass = TBoxMainPass.Password,
                        mport = int.Parse(TBoxMainPort.Text),
                        mtable = TBoxMainTable.Text,
                        sip = TBoxSlaveHost.Text,
                        suser = TBoxSlaveUser.Text,
                        spass = TBoxSlavePass.Password,
                        sport = int.Parse(TBoxSlavePort.Text),
                        stable = TBoxSlaveTable.Text
                    });
                if (rs > 0)
                {
                    Common.Msg(true, "保存成功！");
                    Close();
                }
                else
                {
                    Common.Msg(false, "保存失败！");
                }
            }
            catch (Exception ex)
            {
                Common.Msg(false, "保存失败，信息：" + ex.Message);
            }
        }

        /// <summary>
        ///     清空所有文件本内容
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCanel_OnClick(object sender, RoutedEventArgs e)
        {
            TBoxMainHost.Text = "";
            TBoxMainPort.Text = "";
            TBoxMainUser.Text = "";
            TBoxMainPass.Password = "";
            TBoxMainTable.Text = "";
            TBoxSlaveHost.Text = "";
            TBoxSlavePort.Text = "";
            TBoxSlaveUser.Text = "";
            TBoxSlavePass.Password = "";
            TBoxSlaveTable.Text = "";
        }
    }
}